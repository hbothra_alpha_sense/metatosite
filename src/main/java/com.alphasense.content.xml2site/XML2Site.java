package com.alphasense.content.xml2site;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigParseOptions;
import com.typesafe.config.ConfigRenderOptions;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class XML2Site {

    public static void main(String[] args) throws Exception {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser sp = spf.newSAXParser();
        XMLReader xr = sp.getXMLReader();
        XMLHandler handler = new XMLHandler(xr);
        xr.setContentHandler(handler);
        Path folder = Paths.get(args[0]);
        processFilesFromFolder(folder, xr);
        handler.getElementsWithValues().entrySet().stream().forEach(entry -> System.out.println(entry.getKey() + ": " + entry.getValue()));
        Config config = ConfigFactory.parseMap(handler.getElementsWithValues());
        HtmlGenerator.generateHTMLWithJSON(config.root().render(ConfigRenderOptions.concise()), folder);
    }

    private static void processFilesFromFolder(Path folder, XMLReader xr) throws IOException {
        Files.walk(folder).filter(path -> isXMLfile(path)).forEach(path -> { parseXMLFile(xr, path);});
    }

    private static void parseXMLFile(XMLReader xr, Path path) {
        try {
            xr.parse(path.toFile()
                         .getAbsolutePath());
        } catch (IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    private static boolean isXMLfile(Path path) {
        try {
            return !Files.isDirectory(path) && path.getFileName().toString().toLowerCase().endsWith("xml") && Files.size(path) > 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}
