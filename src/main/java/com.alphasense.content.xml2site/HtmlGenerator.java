package com.alphasense.content.xml2site;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class HtmlGenerator {

    public static void generateHTMLWithJSON(String jsonData, Path path) throws IOException {
        System.out.println(jsonData);
        String htmlTemplate = new String(IOUtils.toByteArray(HtmlGenerator.class.getResourceAsStream("/template.html")));
        htmlTemplate = htmlTemplate.replace("$data", jsonData);
        Path htmlFile = Paths.get(path.toString() + "/dataModel.html");
        Files.write(htmlFile, htmlTemplate.getBytes(StandardCharsets.UTF_8));
    }
}
