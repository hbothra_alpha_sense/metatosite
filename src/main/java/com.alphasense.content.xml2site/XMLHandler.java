package com.alphasense.content.xml2site;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

public class XMLHandler extends DefaultHandler {

    private static String pathString = "";
    private static final Pattern p = Pattern.compile(
                    "[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
    private final XMLReader xmlReader;
    private final StringBuilder characters = new StringBuilder();
    private final Map<String, Map<String, String>> elementsWithValues = new HashMap<>();
    private String fileName;

    public XMLHandler(XMLReader xmlReader) {
        this.xmlReader = xmlReader;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        pathString = String.format("%s%s.", pathString, qName);
        int attrsLength = attrs.getLength();
        for (int x = 0; x < attrsLength; x++) {
            String key = String.format("%s.\"@%s\"", removeLastDot(pathString, false), attrs.getQName(x));
            extracted(attrs.getValue(x), key);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        String value = characters.toString().trim();
        if (value.length() > 0) {
            long spaceCount = value.chars()
                                   .filter(c -> c == 32)
                                   .count();
            String key = removeLastDot(pathString, true);
            if (spaceCount < 3) {
                extracted(value, key);
            }
        }
        characters.setLength(0);
        pathString = pathString.substring(0, pathString.length() - qName.length() - 1);
    }

    private void extracted(String value, String key) {
        if (p.matcher(value)
             .find())
            value = String.format("\"%s\"", value);
        if (null == key || null==value || key.trim().length() < 1 || value.trim().length() < 1)
            return;
        key = escapeString(key);
        if (!elementsWithValues.containsKey(key)) {
            elementsWithValues.put(key, new HashMap<>());
        }
        if (Objects.nonNull(value) &&
            value.trim()
                 .length() > 1)
            value = escapeString(value);
            elementsWithValues.get(key)
                              .put(value, fileName);
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        characters.append(ch, start, length);
    }

    @Override
    public void setDocumentLocator(Locator locator) {
        String systemId = locator.getSystemId();
        Path p = Paths.get(URI.create(systemId));
        fileName = p.getFileName()
                    .toString();
    }

    public Map<String, Map<String, String>> getElementsWithValues() {
        return elementsWithValues;
    }

    private static String removeLastDot(String key, boolean isText) {
        StringBuilder builder = new StringBuilder(key);
        if (key.endsWith("."))
            builder.deleteCharAt(builder.length() - 1);
        if (isText)
            builder.append(".text()");
        return builder.toString();
    }

    private static String escapeString(String str) {
        return str.replace("\t", "\\t")
                  .replace("\n", "\\n")
                  .replace("\r", "\\r")
                  .trim();
    }
}
